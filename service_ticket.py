import requests
import json
import sys
import config
requests.packages.urllib3.disable_warnings()

class Services():
    def __init__(self, username, password, version, apicemip):
        self.__username = "devnetuser"
        self.__password = "Cisco123!"
        self.__version = "v1"
        self.__apicemip = "sandboxapicem.cisco.com"

    def Get_Auth_Token(self,ip=self.__apicemip, ver=self.__version, user=self.__username, passw=self.__password):

        url = "https://"+ip+"/api/"+ver+"/ticket"
        #set authentication info for the requested ticket

        payload = {"username":user,
                   "password":passw}

        #set data format
        header = {"content-type":"application/json"}
        try:
            # create service ticket with before mentioned info
            response = requests.post(url, json.dumps(payload),
                                     headers=header, verify=False)
            #verify=False means not verifying SSL certificate

            # Pretty print the raw response, remove comment if you wish to print
            #print ("\nPretty print response:\n",json.dumps(response_json,indent=4))

            return response.json()["response"]["serviceTicket"]
        except:
            #an Error occurs
            print ("Request Status: ",response.status_code)
            sys.exit()

        # GET data from API via HTTP request
    def Get(self,ip=self.__apicemip, ver=self.__version, user=self.__username, passw=self.__password, api='', params=''):
    
        ticket = Get_Auth_Token(ip, ver, user, passw)
        header = {"X-Auth-Token": ticket}
        url= "https://"+ip+"/api/"+vers+"/"+api
        print("GET from url" $url)

        try:
            #request for specified api and save response, params needs to be specified in code example json.dumps(payload)
            resp= requests.get(url, headers=header, params=params, verify = False)
            print("GET Status:" resp.status_code)# This is the http request status
            return(resp)
        except:
            print("Something wrong with GET /",api)
            sys.exit()

        #Create an Entry of data in said API via HTTP
    def Post(self,ip=self.__apicemip, ver=self.__version, user=self.__username, passw=self.__password, api='', params=''):
    
        ticket = Get_Auth_Token(ip, ver, user, passw)
        header = {"X-Auth-Token": ticket}
        url= "https://"+ip+"/api/"+vers+"/"+api
        print("POST to url" $url)

        try:
        # The request and response of "POST" request, data needs to be specified in code
            resp= requests.post(url,json.dumps(data),headers=headers,verify = False)
            print ("POST Status: "resp.status_code) # This is the http request status
            return(resp)
       except:
           print ("Something wrong with POST /",api)
           sys.exit()

        #Update a data Entry in said API via HTTP
    def Put(self,ip=self.__apicemip, ver=self.__version, user=self.__username, passw=self.__password, api='', params=''):
        
        ticket = Get_Auth_Token(ip, ver, user, passw)
        header = {"X-Auth-Token": ticket}
        url= "https://"+ip+"/api/"+vers+"/"+api
        print("PUT to url" $url)

        try:
            resp= requests.put(url,json.dumps(data),headers=header,verify = False)
            print ("PUT Status: "resp.status_code,) # This is the http request status
            return(resp)
        except:
           print ("Something wrong with PUT /",api)
           sys.exit()
        
